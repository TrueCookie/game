#include "Player.h"

Player::Player() {
    this->strength += this->w.show_strength();
}

bool Player :: is_alive() {
	return !(this->hp <= 0);
}

int Player :: get_health() {
	return hp;
}

int Player :: get_strength() {
	return this->strength;
}

void Player:: get_attacked(int num) {
	this->hp -= num;
}

void Player:: give_weapon(Weapon weapon) {
	this->strength += weapon+1;
}

void Player::fulfill_health() {
	this->hp = 150;
}