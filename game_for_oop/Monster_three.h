#pragma once
#include "Monster.h"
class Monster_three : public Monster {
private:
	bool can_attack = false;
public:
	Monster_three() : Monster() {
		this->set_health(120);
		this->set_strength(1);
	}

	void change_can_attack();

	int attack();

};
