#pragma once
#include "MonsterFactory.h"
#include <ctime>
#include <cstdlib>

class EasyFactory : public MonsterFactory{
    public:
    Monster* createMonster(){
   time_t seconds;
        time(&seconds);
        srand((unsigned)seconds);
        int t = rand()%10;
        if(t<5) //вероятность 50%
        return Monster_one;
        if(t>=5 && t<=7) // вероятность 30%
        return Monster_two;
        if(t>7) //вероятность 20%
        return Monster_three; 
    }
}