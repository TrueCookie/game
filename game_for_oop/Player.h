#pragma once
#include "Weapon.h"

class Player {
private:
	int hp = 150;
    Weapon weapon;
	int strength = 2;
public:

	Player();

	bool is_alive();
	int get_health();
	int get_strength();

	void get_attacked(int num);

	void give_weapon(int weapon);

	void fulfill_health();
};