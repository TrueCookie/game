#include "Monster.h"

Monster::Monster(){
    this->strength += this->w.show_strength();
}

bool Monster :: is_alive() {
	return !(this->hp <= 0);
}

int Monster :: get_health() {
	return this->hp;
}

int Monster :: get_strength() {
	return this->strength;
}

void Monster :: set_health(int num) {
	this->hp = num;
}

void Monster:: set_strength(int num) {
	this->strength = num;
}

void Monster:: get_attacked(int num) {
	this->hp -= num;
}

void Monster :: give_weapon(int weapon) {
	this->strength += weapon+1;
}

void Monster::change_strength(int num) {
	this->strength *= num;
}

int Monster::attack() {
	return this->strength;
}