#pragma once

#include "Weapon.h"

class Monster {
private:
	int hp;
    Weapon weapon;
	int strength;
public:
	Monster() = default;

	bool is_alive();

	int get_health();

	int get_strength();

	void set_health(int num);

	void set_strength(int num);

	void get_attacked(int num);

	void give_weapon(int weapon);

	void change_strength(int num);
	
	virtual int attack();
};
