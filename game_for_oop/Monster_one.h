#pragma once
#include "Monster.h"

class Monster_one : public Monster {
private:
	bool upgrade = false;
public:
	Monster_one() : Monster() {
		this->set_health(120);
		this->set_strength(2);
	}

	bool had_upgrade();

	void do_upgrade();

	int attack();

};
