#pragma once
#include "Monster.h"

class MonsterFactory {
public:
	virtual Monster* createMonster();
	~MonsterFactory() = default;

};
