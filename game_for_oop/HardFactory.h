#pragma once
#include "MonsterFactory.h"
#include <ctime>
#include <cstdlib>

class EasyFactory : public MonsterFactory{
    public:
    Monster* createMonster(){
   time_t seconds;
        time(&seconds);
        srand((unsigned)seconds);
        int t = rand()%10;
        if(t<3) //вероятность 30%
        return Monster_one;
        if(t>=3 && t<=8) // вероятность 60%
        return Monster_two;
        if(t>8) //вероятность 10%
        return Monster_three; 
    }
}