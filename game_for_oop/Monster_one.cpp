#include "Monster_one.h"

bool Monster_one::had_upgrade() {
	return this->upgrade;
}

void Monster_one::do_upgrade() {
	if (this->get_health() <= 10 && this->had_upgrade() == false) {
		this->change_strength(2);
		this->upgrade = true;
	}
}

int Monster_one :: attack(){
	this->do_upgrade();
	return this->get_strength();
}

