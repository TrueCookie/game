#pragma once
#include <ctime>
#include <cstdlib>

enum weapon_kind { COMMON = 0, RARE, LEGENDARY };

class Weapon {
private:
    int strength;
public:
    Weapon() {
        time_t seconds;
        time(&seconds);
        srand((unsigned)seconds);
        switch (rand() % 3) {
            case 0:
                this->strength = 1;
            case 1:
                this->strength = 2;
            case 3:
                this->strength = 3;
        }
    }

    int show_strength() {
        return this->strength;
    }
};
