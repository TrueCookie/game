#include "Monster_two.h"


int Monster_two :: attack() {
	this->set_can_attack_twice();
	return((this->can_attack_twice) ? this->get_strength() : this->get_strength() * 2);
}

void Monster_two :: set_can_attack_twice() {
	if (rand() % 2 == 0)
		can_attack_twice = true;
	else

		can_attack_twice = false;
}