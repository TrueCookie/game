#pragma once
#include "Monster.h"
#include <ctime>
#include <cstdlib>

class Monster_two : public Monster {
private:
	bool can_attack_twice;
public:
	Monster_two() : Monster() {
		time_t seconds;
		time(&seconds);
		srand((unsigned)seconds);
		this->set_health(120);
		this->set_strength(2);
	};

	int attack();

	void set_can_attack_twice();
};
