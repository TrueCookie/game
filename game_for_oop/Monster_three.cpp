#include "Monster_three.h"

void Monster_three :: change_can_attack() {
	this->can_attack = ((this->can_attack == true) ? false : true);
}

int Monster_three :: attack() {
	this->change_can_attack();
	return ((this->can_attack) ? 0 : this->get_strength() * 3);
}
