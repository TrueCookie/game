#pragma once
#include "MonsterFactory.h"
#include "EasyFactory.h"
#include "HardFactory.h"
#include "Monster.h"
#include "Player.h"
#include <vector>
#include <iostream>
using namespace std;



class Game {
private:
	std::vector<Monster*> monsters;
	MonsterFactory *monster_factory;
	int level;
public:
		Game(int level) {
			Player player;
			switch (level) {
                case 1:
                    monster_factory = new EasyFactory();
                case 2:
                    monster_factory = new HardFactory();
			}
            monsters.push_back(monster_factory->createMonster());

			while (!monsters.empty()) {
				for (int i = 0; i < monsters.size(); i++) {
					while (player.is_alive()) {
                        cout << "Players health " << player.get_health() << endl;
                        cout << "Monsters health " << monsters[i]->get_health() << endl;
                        monsters[i]->get_attacked(player.get_strength());
                        if (!monsters[i]->is_alive()) {
                            monsters.erase(monsters.begin() + i);
                            player.fulfill_health();
                            break;
                        }
                        player.get_attacked(monsters[i]->attack());
				    }
				}
		
			}

	}


};
